function gotoPage(name) {
  var hrefParts = window.location.href.split("/");
  window.location.href = hrefParts[0]
    + "/" + (hrefParts[1] || "en")
    + "/" + name;
}


document.addEventListener("DOMContentLoaded", function() {
  // Hide the default partner type option select is opened
  var partnerField = document.getElementById("partner-type");
  var partnerDefault = document.getElementById("default-partner-type");

  partnerField.addEventListener("focus", function() {
    partnerDefault.style.display = "none";
  });

  partnerField.addEventListener("blur", function() {
    partnerDefault.style.display = "block";
  });

    document.getElementById("show-partners").addEventListener("click", function(event) {
      event.preventDefault();
      document.body.classList.add("show-partners");
    });

    document.getElementById("hide-partners").addEventListener("click", function(event) {
      event.preventDefault();
      document.body.classList.remove("show-partners");
    });

    var form = document.getElementById("apply");
    var errorElement = document.getElementById("error-message");
    var SUBMISSION_ERROR = document.getElementById("submission-error").textContent;

    function showError(message) {
      errorElement.textContent = message;
      form.className = form.className + " error";
    }

    form.addEventListener("submit", function(event) {

      event.preventDefault();

      var data = new FormData(form);
      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            gotoPage("thank-you");
          } else {
            if (console && console.error) {
              console.error(data, arguments);
            }
            showError(SUBMISSION_ERROR);
          }
        }
      };

      xhr.open("POST", "https://hooks.zapier.com/hooks/catch/3616879/qanuve/", true);
      xhr.send(data);
    });

  });
