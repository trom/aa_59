## {{standards-placement-heading[Standards placement heading] <span>02</span> Distinction }}

{{standards-placement-body-text[Standards placement body text] Ads should always be recognizable as ads and distinguishable from other content. Ads should clearly be marked with the word “advertisement” or its equivalent.}}
