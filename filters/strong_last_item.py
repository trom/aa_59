def strong_last_item(str):
    str_parts = str.split(',')
    str_parts_length = len(str_parts)
    str_parts[str_parts_length - 1] = ''.join([
        '<strong>',
        str_parts[str_parts_length - 1],
        '</strong>'
    ])
    return ','.join(str_parts)
